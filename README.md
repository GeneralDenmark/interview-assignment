# Eksemple webshop

## Brug

For at installere påkrævede pakker:

```
$ pip install -r requirements.txt
```

For at køre applikationen:

```
$ make run
```
