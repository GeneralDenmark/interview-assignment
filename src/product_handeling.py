import sqlite3, os, errno, uuid
from loremipsum import get_sentences
from random import randint


def generate_products(name, description, price, c):
    uid = str(uuid.uuid4())
    c.execute('INSERT INTO products VALUES (?,?,?,?);', (uid, name, description, price))


def test_generation_of_products(database):
    conn, c = open_db(database)
    c.execute('SELECT * FROM products;')
    if len(c.fetchall()) < 10:
        for index in range(1, 33):
            generate_products('Produkt ' + str(index), ' '.join(get_sentences(5)), randint(50, 500), c)

    close_db(conn)


def open_db(db_location):
    conn = sqlite3.connect(db_location)
    c = conn.cursor()
    c.execute('CREATE TABLE IF NOT EXISTS products('
              'id char(36) PRIMARY KEY, '
              'name varchar(100) NOT NULL, '
              'description varchar(3072), '
              'price varchar(50));')
    return conn, c


def close_db(conn):
    if conn.total_changes > 0:
        conn.commit()

    conn.close()


def get_product(database, id=None):
    conn, c = open_db(database)
    if id:
        c.execute('SELECT name, description, price FROM products WHERE id = (?);', (id,))
    else:
        c.execute('SELECT * FROM products;')
    product = c.fetchall()

    close_db(conn)
    return product

def find_all_images(product_id):
    images = []
    origin = 'src/static/product_images/' + product_id

    try:
        os.makedirs(origin)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    for image in os.listdir(origin):
        if image.endswith(".png") or image.endswith('.jpg') or image.endswith('jpeg'):
            images.append(image)
    return images
