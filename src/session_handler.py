def add_to_session(product_id, session):
    if 'products' not in session:
        session['products'] = {}
    if product_id in session['products']:
        session['products'][product_id] += 1
    else:
        session['products'][product_id] = 1
    session.modified = True


def subtract_from_session(product_id, session):
    if 'products' in session and product_id in session['products']:
        session['products'][product_id] -= 1
        if session['products'][product_id] <= 0:
            session['products'].pop(product_id, None)
        session.modified = True

