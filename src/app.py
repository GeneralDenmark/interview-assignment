from product_handeling import test_generation_of_products, get_product, find_all_images
from flask import Flask, render_template, request, session, flash, redirect, url_for
from session_handler import add_to_session, subtract_from_session



database='src/Databases/products.db'


app = Flask(__name__)
the_active_tab="None"
app.secret_key = 'super secret key'
app.config['SESSION_TYPE'] = 'filesystem'


@app.context_processor
def global_variables():

    return dict(
        tabsy=[{'Home': [False, '/', 'Gå til forside']},
               {'Kurv': [False, '/kurv/', 'Her kan du se din kurv!']},
               {'For tests': [False, '/test/', 'Clear session kan blive gjort her fra']},
               {'Om os': [True, [['Kontakt os','/kontakt/'], ['hvem er vi?', '/hvemervi/'], ['divide', 'divide'], ['Firmaer vi er i sammarbede med', '/sammarbejde/'] ]]}
               ]
    )


@app.route('/')
def front_page():
    test_generation_of_products(database)
    plain_prod = get_product(database)
    products = []
    for prod in plain_prod:
        img = find_all_images(prod[0])
        if 'default.jpg' in img:
            prod += (True,)
        else:
            prod += (False,)
        products.append(prod)

    return render_template('frontpage.html', products=products)


@app.route('/kurv/', methods=['GET', 'POST'])
def kurv_side():
    products = []
    if 'products' in session:
        for product in session['products']:
            id = session['products'][product]
            products.append((id, product) + get_product(database, product)[0])

    add, id = (0, request.form.get('add')) if request.form.get('add') else (1, request.form.get('subtract')) if request.form.get('subtract') else (2,None)

    if request.method == 'POST':
        if add == 0:
            add_to_session(id, session)
        elif add == 1:
            subtract_from_session(id,session)
        else:
            raise Exception('Whoopsy something bad happend, and it isn\'t allowed', status_code=405)
        return redirect(url_for('kurv_side'))

    return render_template('kurv.html', products=products)


@app.route('/product/<id>/', methods=['GET','POST'])
def vare_kort(id):
    name, description, price = get_product(database, id)[0]
    images = find_all_images(id)

    if request.method == 'POST' and 'put_in_basket' in request.form:
        add_to_session(id, session)
        flash('Produkt er tilføjet til kurven!')
        return redirect(url_for('vare_kort', id=id))

    return render_template('varekort.html', id=id, name=name, description=description, price=price, images=images)


@app.route('/test/', methods=['GET',"POST"])
def test():
    if request.method == 'POST':
        if 'clear' in request.form:
            session.clear()

    return render_template('test.html')


@app.errorhandler(Exception)
def errors(error):
    return render_template('error.html',error=error)






